// data validation /2

import UIKit
import GateKeeper

class DataValidationsTableViewController: UITableViewController {
    private let sectionHeaderHeight: CGFloat = 25
    private let cellIdentifier = "TaskCell"
    private let activityIndicator = UIActivityIndicatorView(style: .medium)

    // MARK: - Manage operations

    private var backgroundQueue: OperationQueue = {
        let q = OperationQueue()
        q.name = "GateKeeper.DataValidationOperation.backgroundQueue"
        q.maxConcurrentOperationCount = 1
        q.qualityOfService = .userInitiated
        return q
    }()

    typealias ValidationResults = (success: Int, failure: Int)
    private var results = ValidationResults(success: 0, failure: 0)

    private let validations: [DataValidationOperation] = [
        // shop
        ShopMinimumCategoriesValidation(),
        ParentCategoryExistsValidation(),
        MinimumProductsExistValidation(),
        MinimumPromosExistValidation(),
        MinimumDiscountsExistValidation(),
        ProductsContainImagesValidation(),

        // flight
        SectorScheduleDatesValidation(),
        StationsContainVATNumberValidation(),

        // currencies
        ScheduleCurrencyValidation(),
        OperatingCurrencyMinimumValidation(),
        AcceptedCurrencyMinimumValidation(),
        AcceptedCurrencyCountValidation(),
        ExchangeRateKeysValidation(),
        ExchangeRatesAcrossSectorsValidation(),

        // crew
        CrewDatesValidation(),

        // EMV config
        CardLimitsValidation(),

        // preOrders
        PreOrderPaxIdValidation(),
        PreOrderPrePaidAmountValidation()
    ]

    private var data = [DataValidationOperation.Category: [DataValidationOperation]]()

    private func sortData() {
        data[.crew] = validations.filter({ $0.category == .crew })
        data[.config] = validations.filter({ $0.category == .config })
        data[.currencies] = validations.filter({ $0.category == .currencies })
        data[.emvconfig] = validations.filter({ $0.category == .emvconfig })
        data[.flight] = validations.filter({ $0.category == .flight })
        data[.shop] = validations.filter({ $0.category == .shop })
        data[.preorder] = validations.filter({ $0.category == .preorder })
    }

    private func runValidations() {
        activityIndicator.startAnimating()

        var lastOperation: Operation?
        for (section, validations) in data.sorted(by: { $0.key.rawValue < $1.key.rawValue }) {
            validations.enumerated().forEach { validation in
                let (index, operation) = validation
                startOperation(op: operation, indexPath: IndexPath(item: index, section: section.rawValue))
                lastOperation = operation
            }
        }

        let completionOperation = BlockOperation {
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                let (success, failure) = self.results
                self.alert(title: "Results", message: String(describing: "Succeeded: \(success), Failed: \(failure)"))
            }
        }

        if let lastOp = lastOperation {
            completionOperation.addDependency(lastOp)
            backgroundQueue.addOperation(completionOperation)
        }
    }

    private func updateResults(operation: DataValidationOperation) {
        var (success, failure) = self.results
        operation.status == .passed ? (success += 1) : (failure += 1)
        self.results = ValidationResults(success: success, failure: failure)
    }

    private func startOperation(op: Operation, indexPath: IndexPath) {
        if let dataOperation = op as? DataValidationOperation {
            dataOperation.completionBlock = {
                if dataOperation.isCancelled {
                    return
                }
                self.updateResults(operation: dataOperation)
                DispatchQueue.main.async {
                    self.tableView.reloadRows(at: [indexPath], with: .fade)
                }
            }
        }
        backgroundQueue.addOperation(op)
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        sortData()

        guard sector != nil else {
            alert(title: "State Error", message: String(describing: "Data validations cannot be run until app performs catering sync (download)"))
            return
        }
        setupActivityIndicator()
        runValidations()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)

        backgroundQueue.cancelAllOperations()
    }
}

// MARK: - Table view data source

extension DataValidationsTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return DataValidationOperation.Category.total.rawValue
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let tableSection = DataValidationOperation.Category(rawValue: section), let validationData = data[tableSection] {
            return validationData.count
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let tableSection = DataValidationOperation.Category(rawValue: section), let validationData = data[tableSection], validationData.count > 0 {
            return sectionHeaderHeight
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        configure(cell: cell, indexPath: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: sectionHeaderHeight))
        view.backgroundColor = UIColor.grayscaleLightGrey
        let label = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.bounds.width - 30, height: sectionHeaderHeight))
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = UIColor.black
        label.text = DataValidationOperation.Category(rawValue: section)?.title
        view.addSubview(label)
        return view
    }

    // MARK: - UI helpers

    private func setupActivityIndicator() {
        let barButton = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.setRightBarButton(barButton, animated: true)
    }

    private func icon(for status: DataValidationOperation.Status) -> UIImage? {
        switch status {
        case .passed:
            return UIImage(named: "status")
        case .warning:
            return UIImage(named: "status-yellow")
        case .failed:
            return UIImage(named: "status-red")
        default:
            return UIImage(named: "status-yellow")
        }
    }

    private func configure(cell: UITableViewCell, indexPath: IndexPath) {
        guard
            let tableSection = DataValidationOperation.Category(rawValue: indexPath.section),
            let validation = data[tableSection]?[indexPath.row]
        else { return }
        cell.textLabel?.text = validation.title
        cell.detailTextLabel?.text = validation.detail
        cell.imageView?.image = icon(for: validation.status)
    }
}

// MARK: - Dependencies

extension DataValidationsTableViewController {
    private var sector: Sector? {
        return flightService?.currentFlight.openSector
    }

    private var flightService: FlightServiceProtocol? {
        return Services.flight
    }
}

extension String {
    /// Converts an image literals to UIImage
    ///
    /// - Returns: A UIImage
    func image() -> UIImage? {
        let size = CGSize(width: 30, height: 30)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.set()
        let rect = CGRect(origin: .zero, size: size)
        UIRectFill(CGRect(origin: .zero, size: size))
        (self as AnyObject).draw(in: rect, withAttributes: [.font: UIFont.systemFont(ofSize: 20)])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
