// MVVM

import Foundation
import GateKeeper

protocol SealsViewModelInput: class {
    func sealsUpdated() -> Bool
    func setSeals(_ seals: [Seal]?, isBarsetOpen: Bool)
    func sealIsDuplicate(_ sealNumber: String) -> Bool
    func addSeal(sealNumber: String, type: Seal.SealType)
    func reloadSeals()
    func resetAdjustments()
    func actionRows() -> [Row]
    func loadSections() -> [Section]
}

protocol SealsViewModelOutput: class {
    func showDuplicateSealAlert()
    func showDetails(_ viewController: UIViewController)
    func barCodeScannerStart()
    func popViewController()
    func deleteRows(at indexPath: IndexPath)
    func getIndexPath(for row: Row) -> IndexPath?
    func nextButtonPressed()
}

enum SealsCoordinatorEvent {
    case next
}

protocol SealsCoordinating {
    func on(event: SealsCoordinatorEvent)
}

class SealsViewModel: FormViewModel, SealsViewModelInput {
    private weak var output: SealsViewModelOutput?
    private var seals: [Seal] = []
    private let initialSeals: [Seal]

    private var sealsTypeFilter: [Seal.SealType] {
        return barsetOperation == .open
            ? [.highSecurity, .outbound]
            : [.highSecurity, .inbound]
    }

    private var flightService: FlightServiceProtocol!
    private var inventoryService: InventoryServiceProtocol!
    private var barsetOperation: BarsetOperation

    var coordinator: SealsCoordinating?

    var title: String {
        return L10n.Seals.title
    }

    var subtitle: String {
        return L10n.Seals.subtitle
    }

    init(appState: AppState, barsetOperation: BarsetOperation, output: SealsViewModelOutput) {
        self.barsetOperation = barsetOperation
        self.output = output
        self.initialSeals = appState.flight.seals
        super.init(appState: appState)

        flightService = appState.sharedInstanceContainer.synchronize().resolve(FlightServiceProtocol.self)
        inventoryService = appState.sharedInstanceContainer.synchronize().resolve(InventoryServiceProtocol.self)
    }

    // MARK: - Seals

    func reloadSeals() {
        seals = !flightService.currentFlight.seals.isEmpty
            ? flightService.currentFlight.seals
            : flightService.currentFlight.store?.seals ?? []
    }

    func updateFlightSeals() {
        flightService.setSeals(seals, isBarsetOpen: barsetOperation == .open)
        reloadSeals()
    }

    func sealsUpdated() -> Bool {
        return flightService.currentFlight.seals != initialSeals
    }

    func setSeals(_ seals: [Seal]? = nil, isBarsetOpen: Bool = false) {
        if let seals = seals {
            flightService.setSeals(seals, isBarsetOpen: isBarsetOpen)
        } else {
            flightService.setSeals(initialSeals)
        }
    }

    func addSeal(sealNumber: String, type: Seal.SealType) {
        let seal = Seal(number: sealNumber, type: type, hasBeenTampered: false)
        flightService.add(seal: seal)
        reloadSeals()
    }

    func sealIsDuplicate(_ sealNumber: String) -> Bool {
        return flightService.currentFlight.seals.contains(where: { $0.number == sealNumber })
    }

    func resetAdjustments() {
        inventoryService.resetAdjustments()
    }

    // MARK: - FormKit

    override func loadSections() -> [Section] {
        let rows = seals.of(types: sealsTypeFilter).map { seal -> Row in
            var subtitle = seal.type == .highSecurity ? L10n.Seals.highSecurity : L10n.Seals.standard

            if seal.hasBeenTampered {
                subtitle += " (" + L10n.Seals.damaged + ")"
            }

            return IconTitleAndRightDetailRow(
                title: L10n.Seals.seal + " " + seal.number,
                detailText: subtitle,
                icon: #imageLiteral(resourceName: "icon_seals"),
                accessoryType: .disclosureIndicator,
                editingAccessoryType: .disclosureIndicator,
                action: { [weak self] _ in
                    self?.sealSelected(seal)
                },
                deleteAction: { [weak self] row in
                    guard
                        let strongSelf = self,
                        let indexPath = strongSelf.output?.getIndexPath(for: row),
                        let sealIndex = strongSelf.seals.firstIndex(of: seal)
                        else { return }

                    strongSelf.seals.remove(at: sealIndex)
                    strongSelf.updateFlightSeals()
                    strongSelf.output?.deleteRows(at: indexPath)
                }
            )
        }

        let barcodeButtonClosure: Section.ConfigureButtonClosure = { [unowned self] button, event in
            if event != nil {
                self.output?.barCodeScannerStart()
            }
            button.tintColor = .black
            button.setImage(#imageLiteral(resourceName: "icon_barcode_reader").withRenderingMode(.alwaysTemplate), for: .normal)
        }

        let sealsSection = [
            Section(
                header: L10n.Seals.Title.uppercased,
                configureHeaderButton: barcodeButtonClosure,
                footer: !rows.isEmpty ? "" : L10n.Seals.footer,
                rows: rows
            )
        ]
        return sealsSection
    }

    func actionRows() -> [Row] {
        return [
            ActionRow(title: L10n.Seals.addNew, style: .secondary, action: { [weak self] _ in
                self?.addNewSealPressed()
            }),
            ActionRow(title: L10n.Buttons.next, action: { [weak self] _ in
                self?.nextButtonPressed()
            })
        ]
    }

    // MARK: - Actions

    private func sealSelected(_ seal: Seal) {
        showSealDetails(seal: seal) { [weak self] updatedSeal in
            guard let sealIndex = self?.seals.firstIndex(of: seal) else { return }
            guard (self?.seals.contains { $0.number == updatedSeal.number && seal.number != $0.number && $0.type == updatedSeal.type }) == false else {
                self?.output?.showDuplicateSealAlert()
                return
            }
            self?.seals[sealIndex] = updatedSeal
            self?.updateFlightSeals()
            self?.output?.popViewController()
        }
    }

    private func showSealDetails(
        seal: Seal? = nil,
        commitChangesHandler: @escaping SealDetailViewModel.CommitChangesHandler
    ) {
        let controller = SealDetailViewController()
        controller.viewModel = SealDetailViewModel(
            appState: appState,
            output: controller,
            initialSealType: barsetOperation == .open ? .outbound : .inbound,
            seal: seal,
            commitChangesHandler: commitChangesHandler
        )

        output?.showDetails(controller)
    }

    func addNewSealPressed() {
        showSealDetails { [weak self] seal in
            guard (self?.seals.contains { $0.number == seal.number && $0.type == seal.type }) == false else {
                self?.output?.showDuplicateSealAlert()
                return
            }
            self?.flightService.add(seal: seal)
            self?.output?.popViewController()
        }
    }

    func nextButtonPressed() {
        setSeals(seals, isBarsetOpen: barsetOperation == .open)

        guard let coordinator = coordinator else {
            output?.nextButtonPressed()
            return
        }
        coordinator.on(event: .next)
    }
}

// MARK: - DismissableWithConfirmation
extension SealsViewModel: DismissableWithConfirmation {
    var hasChanges: Bool {
        switch barsetOperation {
        case .close:
            return sealsUpdated()
        case .open:
            return seals.of(types: sealsTypeFilter) != initialSeals.of(types: sealsTypeFilter)
        }
    }

    func discardChanges() {
        switch barsetOperation {
        case .close:
            setSeals()
        case .open:
            seals = initialSeals
            updateFlightSeals()
        }
    }
}
