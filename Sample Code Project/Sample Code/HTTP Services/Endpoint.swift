//
//  Endpoint.swift
//  CryptoCalculator
//
//  Created by Demian Turner on 21/04/2020.
//  Copyright © 2020 Demian Turner. All rights reserved.
//

import Foundation

struct Endpoint {
    var path: String
    var queryItems: [URLQueryItem] = []
}

extension Endpoint {
    var url: URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "min-api.cryptocompare.com"
        components.path = "/" + path
        components.queryItems = queryItems + [
            URLQueryItem(name: "extraParams", value: "crypto_calculator_macos"),
            URLQueryItem(name: "api_key", value: Constants.apiKey)
        ]

        guard let url = components.url else {
            preconditionFailure(
                "Invalid URL components: \(components)"
            )
        }
        log.info(url.absoluteString)
        return url
    }
}

extension Endpoint {
    static func priceMulti(symbols: [String], baseCurrency: String = "USD") -> Self {
        var syms = symbols
        while syms.joined(separator: ",").count > CryptoCompareService.tsymsMaxLength {
            _ = syms.popLast()
        }
        let symbolString = syms.joined(separator: ",")
        return Endpoint(path: "data/pricemulti",
             queryItems: [
                URLQueryItem(
                    name: "fsyms",
                    value: symbolString
                ),
                URLQueryItem(
                    name: "tsyms",
                    value: symbolString
                )
            ]
        )
    }
    
    static func priceMultiFull(symbols: [String], baseCurrency: String = "USD") -> Self {
        let symbolString = symbols.joined(separator: ",")
        return Endpoint(path: "data/pricemultifull",
             queryItems: [
                URLQueryItem(
                    name: "fsyms",
                    value: symbolString
                ),
                URLQueryItem(
                    name: "tsyms",
                    value: baseCurrency
                )
            ]
        )
    }
    
    static func priceSingle(symbol: String, baseCurrency: String = "USD") -> Self {
        return Endpoint(path: "data/price",
             queryItems: [
                URLQueryItem(
                    name: "fsym",
                    value: symbol
                ),
                URLQueryItem(
                    name: "tsyms",
                    value: baseCurrency
                )
            ]
        )
    }
    
    static func allNews(language: String = "EN") -> Self {
        return Endpoint(path: "data/v2/news/",
             queryItems: [
                URLQueryItem(
                    name: "lang",
                    value: language
                ),
            ]
        )
    }
}
