// decoding complex types

import Foundation

public struct CCResponsePriceMultiFull: Decodable {
    var rawData: FromCoin
    
    private enum CodingKeys: String, CodingKey {
        case rawData = "RAW"
    }
    
    struct FromCoin: Decodable {
        private struct OuterKeys: CodingKey {
            var stringValue: String
            init?(stringValue: String) {
                self.stringValue = stringValue
            }
            
            var intValue: Int?
            init?(intValue: Int) {
                self.stringValue = String(intValue)
                self.intValue = intValue
            }
        }
        
        private var fromCoins: [String: ToCoin] = [:]

        var allKeys: [String] = [String]()
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: OuterKeys.self)
            allKeys = container.allKeys.map { $0.stringValue }

            for key in container.allKeys {
                fromCoins[key.stringValue] = try container.decode(ToCoin.self, forKey: key)
            }
        }
        
        subscript(_ key: String) -> ToCoin? {
            return fromCoins[key]
        }
    }
}

struct ToCoin: Decodable {
    struct InnerKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        
        var intValue: Int?
        init?(intValue: Int) {
            self.stringValue = String(intValue)
            self.intValue = intValue
        }
    }
    
    private var toCoins: [String: PriceMultiDict] = [:]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: InnerKeys.self)
        for key in container.allKeys {
            toCoins[key.stringValue] = try container.decode(PriceMultiDict.self, forKey: key)
        }
    }
    
    subscript(_ key: String) -> PriceMultiDict? {
        return toCoins[key]
    }
}

struct PriceMultiDict: Decodable {
    var fromSymbol: String
    var toSymbol: String
    var price: Double
    var change24Hour: Double
    var changePct24Hour: Double
    
    private enum CodingKeys: String, CodingKey {
        case fromSymbol = "FROMSYMBOL"
        case toSymbol = "TOSYMBOL"
        case price = "PRICE"
        case change24Hour = "CHANGE24HOUR"
        case changePct24Hour = "CHANGEPCT24HOUR"
    }
}

