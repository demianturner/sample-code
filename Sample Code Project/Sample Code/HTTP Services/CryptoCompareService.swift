//
//  CryptoCompareService.swift
//  CryptoCalculator
//
//  Created by Demian Turner on 23/01/2018.
//  Copyright © 2018 Demian Turner. All rights reserved.
//

import Foundation

public struct HTTPMethod {
    public static let POST = "POST"
    public static let GET = "GET"
    public static let PUT = "PUT"
    public static let DELETE = "DELETE"
    public static let HEAD = "HEAD"
    public static let PATCH = "PATCH"
}

public enum CCApiError: Error {
    case notFound
    case serverError(Int)
    case requestError
    case responseFormatInvalid(String)
    case connectionError(Error)
}

public class CryptoCompareService {
    public typealias ApiCompletionBlock<T : Decodable> = (Result<T, CCApiError>) -> Void

    public static let timeout = 15.0
    public static let tsymsMaxLength = 100
    private let session: URLSession

    init(session: URLSession) {
        self.session = session
    }
    
    // calculator
    public func checkPriceMulti(
        symbols: [String],
        baseCurrency: String,
        completion: @escaping ApiCompletionBlock<CCResponsePriceMulti>)
    {
        let endpoint: Endpoint = .priceMulti(symbols: symbols, baseCurrency: baseCurrency)
        let request = makeRequest(endpoint: endpoint.url)
        
        doRequest(request, completion: completion)
    }
    
    // ticker
    public func checkPriceMultiFull(
        symbols: [String],
        baseCurrency: String,
        completion: @escaping ApiCompletionBlock<CCResponsePriceMultiFull>)
    {
        let endpoint: Endpoint = .priceMultiFull(symbols: symbols, baseCurrency: baseCurrency)
        let request = makeRequest(endpoint: endpoint.url)
        
        doRequest(request, completion: completion)
    }
    
    // BTC in status bar
    public func checkPriceSingle(
        symbol: String,
        completion: @escaping ApiCompletionBlock<CCResponsePriceSingle>)
    {
        let endpoint: Endpoint = .priceSingle(symbol: "BTC")
        let request = makeRequest(endpoint: endpoint.url)
        
        doRequest(request, completion: completion)
    }
    
    // news
    public func latestNews(
        language: String = "EN",
        completion: @escaping ApiCompletionBlock<CCLatestNews>)
    {
        let endpoint: Endpoint = .allNews()
        let request = makeRequest(endpoint: endpoint.url)
        
        doRequest(request, completion: completion)
    }
    
    private func makeRequest(endpoint: URL) -> URLRequest {
        var request = URLRequest(url: endpoint)
        request.timeoutInterval = CryptoCompareService.timeout
        request.httpMethod = HTTPMethod.GET
        request.cachePolicy = .reloadIgnoringLocalCacheData
        return request
    }
    
    private func doRequest<T>(_ request: URLRequest, completion: @escaping ApiCompletionBlock<T>) {
        let task = session.dataTask(with: request) { maybeData, response, error in
            if let e = error {
                DispatchQueue.main.async {
                    completion(.failure(.connectionError(e)))
                }
            } else {
                let http = response as! HTTPURLResponse
                switch http.statusCode {
                case 200:
                    guard let data = maybeData else {
                        DispatchQueue.main.async {
                            let bodyString = String(data: maybeData!, encoding: .utf8)
                            completion(.failure(.responseFormatInvalid(bodyString ?? "<no body>")))
                        }
                        return
                    }
                    let decoder = JSONDecoder()
                    do {
                        let decoded = try decoder.decode(T.self, from: data)
                        DispatchQueue.main.async {
                            completion(.success(decoded))
                        }
                    } catch let e {
                        log.info("Invalid JSON \(e)")
                        DispatchQueue.main.async {
                            let bodyString = String(data: data, encoding: .utf8)
                            completion(.failure(.responseFormatInvalid(bodyString ?? "<no body>")))
                        }
                    }
                default:
                    DispatchQueue.main.async {
                        completion(.failure(.serverError(http.statusCode)))
                    }
                }
            }
        }
        task.resume()
    }
}
