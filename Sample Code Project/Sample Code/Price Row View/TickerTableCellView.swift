// customer view /2

import Cocoa

class TickerTableCellView: NSTableCellView {

    @IBOutlet weak var currencyImageView: NSImageView!
    @IBOutlet weak var currencyCode: NSTextField!
    @IBOutlet weak var currencyName: NSTextField!
    @IBOutlet weak var currencyAmount: TableViewTextField!
    
    let darkColor = NSColor.secondaryLabelColor
    let mediumColor = NSColor.secondaryLabelColor
    let lightColor = NSColor.white
    
    override var backgroundStyle: NSView.BackgroundStyle {
        willSet {
            super.backgroundStyle = newValue
            updateTextBackgroundStyle(textBackgroundStyle: newValue)
        }
    }
    
    func setup(image: NSImage?, name: String, code: String, price: String, change: Double) {
        currencyImageView.image = image
        currencyCode.stringValue = code
        currencyName.stringValue = name
        currencyAmount.stringValue = price

        subviews.forEach { view in
            if view is RateChangeView {
                view.removeFromSuperview()
            }
        }
        let rateView = RateChangeView(frame: .zero, percentage: change)
        addSubview(rateView)
        rateView.translatesAutoresizingMaskIntoConstraints = false
        
        // layout
        let views: [String: Any] = [
            "rateView": rateView]
        var allConstraints: [NSLayoutConstraint] = []
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-12-[rateView]-12-|",
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let horizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-240-[rateView(68)]-10-|",
            metrics: nil,
            views: views)
        allConstraints += horizontalConstraints
        
        NSLayoutConstraint.activate(allConstraints)
    }
    
    func updateTextBackgroundStyle(textBackgroundStyle: NSView.BackgroundStyle) {
        if textBackgroundStyle == .dark {
            currencyCode.textColor = lightColor
            currencyName.textColor = lightColor
        } else {
            currencyCode.textColor = darkColor
            currencyName.textColor = mediumColor
        }
    }    
}
