// customer view /1

import Cocoa

class RateChangeView: NSView {

    var percentage: Double
    var currentColour: NSColor?

    private lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.minimumIntegerDigits = 1
        return formatter
    }()
    
    init(frame frameRect: NSRect, percentage: Double) {
        self.currentColour = percentage < 0 ? Constants.redColor : Constants.greenColor
        self.percentage = percentage
        super.init(frame: frameRect)
    }
    
    required init?(coder decoder: NSCoder) {
        fatalError("not implemented")
    }
    
    func drawTextInContext(_ context: CGContext?) {
        let textColor: NSColor = .white
        let textFont = NSFont.systemFont(ofSize: 14, weight: .light)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .right
        let nameTextAttributes: [NSAttributedString.Key : Any] = [
            .font: textFont,
            .paragraphStyle: paragraphStyle,
            .foregroundColor: textColor
        ]
        let pct = formatter.string(for: percentage) ?? ""
        let str = "\(pct)%"
        var bounds = self.bounds // vertically centre text
        bounds.origin.y -= 5
        bounds.origin.x -= 5
        str.draw(in: bounds, withAttributes: nameTextAttributes)
    }
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        
        self.wantsLayer = true
        self.layer?.backgroundColor = currentColour?.cgColor
        self.layer?.cornerRadius = 4.0
        
        let context = NSGraphicsContext.current?.cgContext
        drawTextInContext(context)
    }
}
