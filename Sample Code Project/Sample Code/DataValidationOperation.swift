// data validation /1

import Foundation
import RealmSwift

//    ┌────────┐        ┌──────────┐           ┌──────────┐           ┌──────────┐    ┌──────────┐
//    │ Import │        │Preflight │           │ InFlight │           │   Post   │    │  Export  │
//    └────────┘        └──────────┘           └──────────┘           └──────────┘    └──────────┘
//    ┌──────────────┐ ┌─────────────┐ ┌───────────────────────────┐  ┌──────────┐  ┌──────────────┐
//    │              │ │             │ │                           │  │          │  │              │
//    │              │ │             │ │                           │  │          │  │              │
//    │              │ │             │ │    shop transactions,     │  │          │  │              │
//    │              │ │             │ │      review orders,       │  │          │  │              │
//    │  download,   │ │             │ │         refunds,          │  │  close   │  │upload JSON to│
//    │ parse, store │ │flight setup │ │         targets,          │  │  flight  │  │    bucket    │
//    │   in Realm   │ │             │ │         reports,          │  │          │  │              │
//    │              │ │             │ │         settings,         │  │          │  │              │
//    │              │ │             │ │      sync to devices      │  │          │  │              │
//    │              │ │             │ │                           │  │          │  │              │
//    │              │ │             │ │                           │  │          │  │              │
//    └──────────────┘ └─────────────┘ └───────────────────────────┘  └──────────┘  └──────────────┘

public protocol DataValidationProviding {
    var title: String { get }
    var detail: String { get }
    var category: DataValidationOperation.Category { get }
}

public class DataValidationOperation: Operation, DataValidationProviding {
    public var title: String {
        return ""
    }

    public var detail: String {
        return ""
    }

    public var status: Status = .notRun

    public enum Status {
        case passed
        case failed
        case warning
        case notRun
    }

    public var appState: AppState!

    public var category: Category {
        return .shop
    }

    public enum Category: Int {
        case crew = 0, config, currencies, emvconfig, flight, shop, preorder, total

        public var title: String {
            switch self {
            case .shop:
                return "Shop"
            case .flight:
                return "Flight"
            case .currencies:
                return "Currencies"
            case .crew:
                return "Crew"
            case .config:
                return "Config"
            case .emvconfig:
                return "EMV Config"
            case .preorder:
                return "PreOrders"
            default:
                return ""
            }
        }
    }

    public init(appState: AppState = Services.appState) {
        self.appState = appState
    }
}

// MARK: - Currencies

// Ensure an OC exists for the Schedule OC
public class ScheduleCurrencyValidation: DataValidationOperation {
    public override var title: String {
        return  "Schedules have existing operating currency"
    }
    public override var detail: String {
        return "Ensure an OC exists for the Schedule OC"
    }
    public override var category: Category {
        return .currencies
    }

    public override func main() {
        guard let sector = sector else { return }

        let scheduleOperatingCurrencyCode = sector.schedule.operatingCurrencyCode
        let operatingCurrencies = CurrenciesService(appState: appState).fetchAvailableOperatingCurrencies()

        let currencies = operatingCurrencies.filter { $0.currency.code == scheduleOperatingCurrencyCode }
        log("base currency exists and is: \(currencies.first?.currency.code ?? "undefined")")
        status = currencies.count > 0
            ? .passed
            : .failed
    }

    private var sector: Sector? {
        return flightService?.currentFlight.openSector
    }

    private var flightService: FlightServiceProtocol? {
        return appState.sharedInstanceContainer.synchronize().resolve(FlightServiceProtocol.self)
    }
}

// At least one OC exists
public class OperatingCurrencyMinimumValidation: DataValidationOperation {
    public override var title: String {
        return  "At least one OC exists"
    }
    public override var detail: String {
        return "A flight needs at least one OC"
    }
    public override var category: Category {
        return .currencies
    }

    public override func main() {
        let currencyCount = CurrenciesService(appState: appState).fetchAvailableOperatingCurrencies().count

        log("operating currencies counted: \(currencyCount)")
        status = currencyCount > 0
            ? .passed
            : .failed
    }
}

// Each OC has at least one AC
public class AcceptedCurrencyMinimumValidation: DataValidationOperation {
    public override var title: String {
        return  "Each OC has at least one AC"
    }
    public override var detail: String {
        return "Operating currencies must have one or more ACs"
    }
    public override var category: Category {
        return .currencies
    }

    public override func main() {
        let operatingCurrencies = CurrenciesService(appState: appState).fetchAvailableOperatingCurrencies()

        for oc in operatingCurrencies {
            let acCount = oc.acceptedCurrencies.count
            if acCount > 0 {
                log("operating currency \(oc.currency.code) has \(acCount) accepted currencies")
                continue
            } else {
                log("operating currency \(oc.currency.code) has no accepted currencies")
                return
            }
        }
        status = .passed
    }
}

// Each OC must have same count of ACs
public class AcceptedCurrencyCountValidation: DataValidationOperation {
    public override var title: String {
        return  "AC counts match"
    }
    public override var detail: String {
        return "Each OC must have same count of ACs"
    }
    public override var category: Category {
        return .currencies
    }

    public override func main() {
        let operatingCurrencies = CurrenciesService(appState: appState).fetchAvailableOperatingCurrencies()

        guard let firstOC = operatingCurrencies.first else { return }
        let acCountControl = firstOC.acceptedCurrencies.count
        for oc in operatingCurrencies {
            let acCountTest = oc.acceptedCurrencies.count
            if acCountTest == acCountControl {
                continue
            } else {
                log("wrong AC count found for: \(oc.currency.code)")
                return
            }
        }
        log("all AC counts identical: \(acCountControl)")
        status = .passed
    }
}

// When AC and OC keys match, the exchange rate must be 1
public class ExchangeRateKeysValidation: DataValidationOperation {
    public override var title: String {
        return  "Exchange rate keys default value"
    }
    public override var detail: String {
        return "When OC/AC keys match, exchange rate must be 1"
    }
    public override var category: Category {
        return .currencies
    }

    typealias ExchangeRates = [String: [String: Decimal]]

    public override func main() {
        let operatingCurrencies = CurrenciesService(appState: appState).fetchAvailableOperatingCurrencies()
        let ratesPerOc = operatingCurrencies.reduce(into: ExchangeRates()) { (out, oc) in
            let key = oc.currency.code
            let rates = oc.acceptedCurrencies.reduce(into: [String: Decimal](), { (dict, c) in
                dict[c.code] = c.exchangeRate
            })
            out[key] = rates
        }

        for (ocCode, acRates) in ratesPerOc {
            let acDuplicateOfOC = acRates.filter { $0.key == ocCode }
            if acDuplicateOfOC[ocCode] == 1 {
                continue
            } else {
                log("AC duplicate of OC \(ocCode) has incorrect exchange rate of \(String(describing: acDuplicateOfOC[ocCode]))")
                status = .failed
                return
            }
        }
        log("all AC duplicates of OC have exchange rate of 1")
        status = .passed
    }
}

// Exchange rates must be the same across sectors
public class ExchangeRatesAcrossSectorsValidation: DataValidationOperation {
    public override var title: String {
        return  "Exchange rates match across sectors"
    }
    public override var detail: String {
        return "Exchange rates must be the same across sectors"
    }
    public override var category: Category {
        return .currencies
    }

    typealias CurrencyCode = String
    typealias RatesPerOC = [CurrencyCode: [CurrencyCode: Decimal]]
    typealias RateListPerOC = [CurrencyCode: [Decimal]]

    public override func main() {
        guard
            let sectors = flightService?.currentFlight.sectors,
            sectors.count > 1 else {
                log("same exchange rates can only be checked on flights with > 1 sector, pass anyway")
                status = .passed
                return
        }

        var results = [RateListPerOC]()
        var ocCount: Int = 0
        sectors.forEach { sector in

            let operatingCurrencies = currenciesService.availableOperatingCurrencies(for: sector.date)
            ocCount = operatingCurrencies.count
            let ratesPerOc = operatingCurrencies.reduce(into: RatesPerOC()) { (out, oc) in
                let key = oc.currency.code
                let rates = oc.acceptedCurrencies.reduce(into: [CurrencyCode: Decimal](), { (innerDict, c) in
                    innerDict[c.code] = c.exchangeRate
                })
                out[key] = rates
            }
            for (key, rateDicts) in ratesPerOc {
                let rates = rateDicts.values.compactMap { $0 }.sorted(by: <)
                let dict = [key: rates]
                results.append(dict)
            }
        }
        let uniqueRatesPerOC = Set(results)
        log("cross sector currency check: each OC key should appear only once")
        uniqueRatesPerOC.forEach {
            guard
                let ocKey = $0.keys.first,
                let rates = $0.values.first
            else { return }
            let formattedRates = rates.map { $0.formatted }
            log("oc: \(ocKey), rates: \(formattedRates)")
        }
        let finalCount = uniqueRatesPerOC.count
        log("oc count: \(ocCount); unique rates per oc: \(finalCount)")

        status = (ocCount == finalCount)
            ? .passed
            : .failed
    }

    private var flightService: FlightServiceProtocol? {
        return appState.sharedInstanceContainer.synchronize().resolve(FlightServiceProtocol.self)
    }

    private var currenciesService: CurrenciesServiceProtocol {
        return appState.sharedInstanceContainer.synchronize().resolve(CurrenciesServiceProtocol.self)!
    }
}

extension Decimal {
    static let fourFractionalDigits: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .decimal
        formatter.minimumFractionDigits = 4
        formatter.maximumFractionDigits = 4
        return formatter
    }()

    var formatted: String {
        return Decimal.fourFractionalDigits.string(for: self) ?? ""
    }
}

// MARK: - Shop

// The shop must have at least one category
public class ShopMinimumCategoriesValidation: DataValidationOperation {
    public override var title: String {
        return  "Minimum categories exist"
    }
    public override var detail: String {
        return "The shop must have at least one category"
    }
    public override var category: Category {
        return .shop
    }

    public override func main() {
        let categoryService = appState.sharedInstanceContainer.synchronize().resolve(CategoryServiceProtocol.self)
        let categoryCount = categoryService?.allCategories.count ?? 0

        log("shop categories counted: \(categoryCount)")
        status = categoryCount > 0
            ? .passed
            : .failed
    }
}

// At least one parent shop category must exist
public class ParentCategoryExistsValidation: DataValidationOperation {
    public override var title: String {
        return  "Category parents exists"
    }
    public override var detail: String {
        return "At least one parent category must exist"
    }
    public override var category: Category {
        return .shop
    }

    public override func main() {
        guard let categories = categoryService?.allCategories else { return }
        let categoryIds = Set(categories.map { $0.categoryId })
        let parentIds = Set(categories.compactMap { $0.parent?.categoryId })
        let intersection = categoryIds.intersection(parentIds)
        log("number of parent category IDs mapped by categories: \(intersection.count)")

        status = intersection.count > 0
            ? .passed
            : .failed
    }

    private var categoryService: CategoryServiceProtocol? {
        return appState.sharedInstanceContainer.synchronize().resolve(CategoryServiceProtocol.self)
    }
}

// The shop must have at least one product
public class MinimumProductsExistValidation: DataValidationOperation {
    public override var title: String {
        return  "Minimum products exist"
    }
    public override var detail: String {
        return "The shop must have at least one product"
    }
    public override var category: Category {
        return .shop
    }

    public override func main() {
        let items = appState.flight.items.map { $0.item }

        log("shop items counted: \(items.count)")
        status = items.count > 0
            ? .passed
            : .failed
    }
}

// The shop must have at least one promo
public class MinimumPromosExistValidation: DataValidationOperation {
    public override var title: String {
        return  "Minimum promos exist"
    }
    public override var detail: String {
        return "The shop must have at least one promo"
    }
    public override var category: Category {
        return .shop
    }

    public override func main() {
        guard let categories = categoryService?.allCategories, let promoService = promoService else { return }

        let promos = categories.map { promoService.promotions(for: $0) }.flatMap { $0 }

        log("shop promos counted: \(promos.count)")
        status = promos.count > 0
            ? .passed
            : .failed
    }

    private var promoService: PromotionsServiceProtocol? {
        return appState.sharedInstanceContainer.synchronize().resolve(PromotionsServiceProtocol.self)
    }

    private var categoryService: CategoryServiceProtocol? {
        return appState.sharedInstanceContainer.synchronize().resolve(CategoryServiceProtocol.self)
    }
}

// The shop must have at least one discount
public class MinimumDiscountsExistValidation: DataValidationOperation {
    public override var title: String {
        return  "Minimum discounts exist"
    }
    public override var detail: String {
        return "The shop must have at least one discount"
    }
    public override var category: Category {
        return .shop
    }

    public override func main() {
        guard let sector = sector, let discountService = discountService else { return }
        let baseCurrency = currenciesService.baseCurrency

        let discounts = discountService.coupons(operatingCurrency: baseCurrency, sector: sector)

        log("shop discounts counted: \(discounts.count)")
        status = discounts.count > 0
            ? .passed
            : .failed
    }

    private var sector: Sector? {
        return flightService?.currentFlight.openSector
    }

    private var flightService: FlightServiceProtocol? {
        return appState.sharedInstanceContainer.synchronize().resolve(FlightServiceProtocol.self)
    }

    private var currenciesService: CurrenciesServiceProtocol {
        return appState.sharedInstanceContainer.synchronize().resolve(CurrenciesServiceProtocol.self)!
    }

    private var discountService: DiscountServiceProtocol? {
        return appState.sharedInstanceContainer.synchronize().resolve(DiscountServiceProtocol.self)
    }
}

// Shop products (items) must have images
public class ProductsContainImagesValidation: DataValidationOperation {
    public override var title: String {
        return  "All products have images"
    }
    public override var detail: String {
        return "Shop products (items) must have images"
    }
    public override var category: Category {
        return .shop
    }

    public override func main() {
        let items = appState.flight.items.map { $0.item }

        var imageData = [Data]()
        let realm = appState.factoryContainer.synchronize().resolve(Realm.self, argument: Realm.Configuration.domain)!
        let imageDBOs = realm.objects(ImageDBO.self)
        items.forEach {
            if let image = getImageData(for: $0.itemId, imageData: imageDBOs) {
                imageData.append(image)
            }
        }

        log("shop items counted: \(items.count), shop images counted: \(imageData.count)")
        status = items.count ==  imageData.count
            ? .passed
            : .failed
    }

    private func getImageData(for itemId: Int, imageData: Results<ImageDBO>) -> Data? {
        let img = imageData
            .filter { $0.itemIds.contains(itemId) }
            .first
        return img?.data
    }
}

// MARK: - EMV Config

public class CardLimitsValidation: DataValidationOperation {
    public override var title: String {
        return  "Correct contactless card limits"
    }
    public override var detail: String {
        return "Contactless card limits are same per card type/currency"
    }
    public override var category: Category {
        return .emvconfig
    }

    public override func main() {
        let seconds: UInt32 = 1
        sleep(seconds)
        status = .passed
    }
}

// MARK: - Flight

// Stations must have valid tax number
public class StationsContainVATNumberValidation: DataValidationOperation {
    public override var title: String {
        return  "Stations must have valid VAT number"
    }
    public override var detail: String {
        return "Sometimes it's not set"
    }
    public override var category: Category {
        return .flight
    }

    public override func main() {
        guard let sectors = flightService?.currentFlight.sectors else {
            status = .failed
            log("no sectors found")
            return
        }

        let stations = sectors.map { $0.schedule.departureStation }

        guard stations.count > 0 else {
            status = .failed
            log("no stations found")
            return
        }
        let vatNumbers = stations.map {
            vatNumberProvider.getVATNumberFor(stationName: $0, effectiveDate: Date())
        }
        log("VAT records found: \(vatNumbers.count), for \(stations.count) station/s across \(sectors.count) sector/s, with missing values: \(vatNumbers.filter { $0 == nil }.count)")
        status = vatNumbers.allSatisfy { $0 != nil }
            ? .passed
            : .failed
    }

    private var flightService: FlightServiceProtocol? {
        return appState.sharedInstanceContainer.synchronize().resolve(FlightServiceProtocol.self)
    }

    private var vatNumberProvider: VATNumberProviding {
        return appState.sharedInstanceContainer.synchronize().resolve(VATNumberProviding.self)!
    }
}

public class SectorScheduleDatesValidation: DataValidationOperation {
    public override var title: String {
        return  "Arrival/dept dates match flight date"
    }
    public override var detail: String {
        return "tbd"
    }
    public override var category: Category {
        return .flight
    }

    public override func main() {
        let seconds: UInt32 = 1
        sleep(seconds)
        status = .passed
    }
}

// MARK: - Crew

public class CrewDatesValidation: DataValidationOperation {
    public override var title: String {
        return  "Valid start/end dates for crew"
    }
    public override var detail: String {
        return "tbd"
    }
    public override var category: Category {
        return .crew
    }

    public override func main() {
        let seconds: UInt32 = 1
        sleep(seconds)
        status = .passed
    }
}

// MARK: - PreOrders

public class PreOrderPaxIdValidation: DataValidationOperation {
    public override var title: String {
        return  "Preorders must contain paxId"
    }
    public override var detail: String {
        return "paxId is basically a SIN and is mandatory for order validation"
    }
    public override var category: Category {
        return .preorder
    }

    public override func main() {
        let preOrders = preOrderProvider?.preOrders(fulfillmentState: .pending)

        var validHomePaidOrders = 0
        var invalidHomePaidOrders = 0
        preOrders?.forEach {
            if $0.paymentStatus == .homePaid {
                guard let paxId = $0.paxId, !paxId.isEmpty else {
                    invalidHomePaidOrders += 1
                    return
                }
                validHomePaidOrders += 1
            }
        }

        log("preOrders counted: \(preOrders?.count ?? 0), valid home orders counted: \(validHomePaidOrders), invalid home orders counted: \(invalidHomePaidOrders)")
        status = invalidHomePaidOrders > 0
            ? .failed
            : .passed
    }

    private var preOrderProvider: PreOrderProviding? {
        return appState.sharedInstanceContainer.synchronize().resolve(PreOrderProviding.self)
    }
}

public class PreOrderPrePaidAmountValidation: DataValidationOperation {

    public override var title: String {
        return  "Pre-paid amounts are correct"
    }

    public override var detail: String {
        return "The sum of pre-paid preorder items should be equal to the pre-paid amount of the preorder"
    }

    public override var category: Category {
        return .preorder
    }

    public override func main() {
        guard let preOrders = preOrderProvider?.preOrders(fulfillmentState: .pending) else {
            status = .failed
            return
        }

        let invalidPrePaidPreOrderNumbers = preOrders
            .compactMap { (preOrder: PreOrder) -> String? in
                guard
                    let prePaidAmount = preOrder.prePaid?.amount,
                    preOrder.paymentStatus != .paid
                else {
                    return nil
                }

                let prePaidItemPriceSum = preOrder.items
                    .filter { preOrder.prepaidItemCodes.contains($0.item.itemCode) }
                    .map { $0.linePrice }
                    .reduce(0, +)

                guard prePaidAmount != prePaidItemPriceSum else {
                    return nil
                }

                return preOrder.preOrderNumber
            }

        log("PreOrderPrePaidAmountValidation - Invalid preorder numbers: [\(invalidPrePaidPreOrderNumbers.joined(separator: ", "))]")

        status = invalidPrePaidPreOrderNumbers.isEmpty ? .passed : .failed
    }

    private var preOrderProvider: PreOrderProviding? {
        return appState.sharedInstanceContainer.synchronize().resolve(PreOrderProviding.self)
    }
}
