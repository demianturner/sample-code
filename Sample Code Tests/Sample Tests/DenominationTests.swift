import XCTest

@testable import GateKeeper

class DenominationTests: XCTestCase {

    let rounder = DenominationRounder()

    func testDeriveMultiplierIntegral() {
        let multiplier = DenominationRounder.Multiplier(rawValue: 1.0)!
        let derived = rounder.deriveMultiplier(from: multiplier.rawValue)

        if case .integral = derived {
            XCTAssertEqual(multiplier.rawValue, derived.rawValue)
        } else {
            XCTFail("Unexpected multiplier")
        }
    }

    func testDeriveMultiplierTwo() {
        let multiplier = DenominationRounder.Multiplier(rawValue: 2.0)!
        let derived = rounder.deriveMultiplier(from: multiplier.rawValue)

        if case .two = derived {
            XCTAssertEqual(multiplier.rawValue, derived.rawValue)
        } else {
            XCTFail("Unexpected multiplier")
        }
    }

    // swiftlint:disable comma
    typealias Input = (input: Decimal, multiplier: DenominationRounder.Multiplier, rounded: Decimal)

    let roundingUp: [Input] = [
        // 0.01
        (input: 1.233, multiplier: .hundredth, rounded: 1.24),
        (input: 1.25,  multiplier: .hundredth, rounded: 1.25),
        (input: 1.251, multiplier: .hundredth, rounded: 1.26),
        (input: 1.237, multiplier: .hundredth, rounded: 1.24),

        // 0.5
        (input: 2.4,   multiplier: .half, rounded: 2.5),
        (input: 2.6,   multiplier: .half, rounded: 3.0),
        (input: 3.5,   multiplier: .half, rounded: 3.5),
        (input: 3.71,  multiplier: .half, rounded: 4.0),

        // 1
        (input: 1.23,  multiplier: .integral, rounded: 2),
        (input: 1.50,  multiplier: .integral, rounded: 2),
        (input: 1.57,  multiplier: .integral, rounded: 2),

        // 2
        (input: 1.23,  multiplier: .two, rounded: 2),
        (input: 1.50,  multiplier: .two, rounded: 2),
        (input: 1.57,  multiplier: .two, rounded: 2),
        (input: 2.0,   multiplier: .two, rounded: 2),
        (input: 2.1,   multiplier: .two, rounded: 4),
        (input: 2.9,   multiplier: .two, rounded: 4),
        (input: 3.4,   multiplier: .two, rounded: 4),

        // 5
        (input: 2.49,  multiplier: .five, rounded: 5),
        (input: 2.5,   multiplier: .five, rounded: 5),
        (input: 2.51,  multiplier: .five, rounded: 5),
        (input: 7.50,  multiplier: .five, rounded: 10),

        // 50
        (input: 46,    multiplier: .fifty, rounded: 50),
        (input: 50,    multiplier: .fifty, rounded: 50),
        (input: 57,    multiplier: .fifty, rounded: 100),
        (input: 103,   multiplier: .fifty, rounded: 150),
    ]

    func testDenominationRoundingUp() {
        // from "content.companies" key in JSON
        let rule: Decimal.RoundingMode = .up
        roundingUp.forEach { i in
            XCTAssertEqual(rounder.applyRounding(for: i.input, denomination: i.multiplier, roundingRule: rule), i.rounded)
        }
    }

    let roundingDown: [Input] = [
        // 0.01
        (input: 1.233, multiplier: .hundredth, rounded: 1.23),
        (input: 1.25,  multiplier: .hundredth, rounded: 1.25),
        (input: 1.251, multiplier: .hundredth, rounded: 1.25),
        (input: 1.237, multiplier: .hundredth, rounded: 1.23),

        // 0.5
        (input: 2.4,   multiplier: .half, rounded: 2),
        (input: 2.6,   multiplier: .half, rounded: 2.5),
        (input: 3.5,   multiplier: .half, rounded: 3.5),
        (input: 3.71,  multiplier: .half, rounded: 3.5),

        // 1
        (input: 1.23,  multiplier: .integral, rounded: 1),
        (input: 1.50,  multiplier: .integral, rounded: 1),
        (input: 1.57,  multiplier: .integral, rounded: 1),

        // 2
        (input: 1.23,  multiplier: .two, rounded: 0),
        (input: 1.50,  multiplier: .two, rounded: 0),
        (input: 1.57,  multiplier: .two, rounded: 0),
        (input: 2.0,   multiplier: .two, rounded: 2),
        (input: 2.1,   multiplier: .two, rounded: 2),
        (input: 2.9,   multiplier: .two, rounded: 2),
        (input: 3.4,   multiplier: .two, rounded: 2),

        // 5
        (input: 2.49,  multiplier: .five, rounded: 0),
        (input: 2.5,   multiplier: .five, rounded: 0),
        (input: 2.51,  multiplier: .five, rounded: 0),
        (input: 7.50,  multiplier: .five, rounded: 5),

        // 50
        (input: 46,    multiplier: .fifty, rounded: 0),
        (input: 50,    multiplier: .fifty, rounded: 50),
        (input: 57,    multiplier: .fifty, rounded: 50),
        (input: 103,   multiplier: .fifty, rounded: 100),
    ]

    func testDenominationRoundingDown() {
        let rule: Decimal.RoundingMode = .down
        roundingDown.forEach { i in
            XCTAssertEqual(rounder.applyRounding(for: i.input, denomination: i.multiplier, roundingRule: rule), i.rounded)
        }
    }

    let roundingPlain: [Input] = [
        // 0.01
        (input: 1.233, multiplier: .hundredth, rounded: 1.23),
        (input: 1.25,  multiplier: .hundredth, rounded: 1.25),
        (input: 1.251, multiplier: .hundredth, rounded: 1.25),
        (input: 1.237, multiplier: .hundredth, rounded: 1.24),

        // 0.5
        (input: 2.4,   multiplier: .half, rounded: 2.5),
        (input: 2.6,   multiplier: .half, rounded: 2.5),
        (input: 3.5,   multiplier: .half, rounded: 3.5),
        (input: 3.71,  multiplier: .half, rounded: 3.5),
        (input: 3.74,  multiplier: .half, rounded: 3.5),
        (input: 3.75,  multiplier: .half, rounded: 4),

        // 1
        (input: 1.23,  multiplier: .integral, rounded: 1),
        (input: 1.50,  multiplier: .integral, rounded: 2),
        (input: 1.57,  multiplier: .integral, rounded: 2),
        (input: Decimal(-0.49871399999993571), multiplier: .integral, rounded: 0),

        // 2
        (input: 1.23,  multiplier: .two, rounded: 2),
        (input: 1.50,  multiplier: .two, rounded: 2),
        (input: 1.57,  multiplier: .two, rounded: 2),
        (input: 2.0,   multiplier: .two, rounded: 2),
        (input: 2.1,   multiplier: .two, rounded: 2),
        (input: 2.9,   multiplier: .two, rounded: 2),
        (input: 3.4,   multiplier: .two, rounded: 4),

        // 5
        (input: 2.49,  multiplier: .five, rounded: 0),
        (input: 2.5,   multiplier: .five, rounded: 5),
        (input: 2.51,  multiplier: .five, rounded: 5),
        (input: 7.50,  multiplier: .five, rounded: 10),

        // 50
        (input: 46,    multiplier: .fifty, rounded: 50),
        (input: 50,    multiplier: .fifty, rounded: 50),
        (input: 57,    multiplier: .fifty, rounded: 50),
        (input: 103,   multiplier: .fifty, rounded: 100),
    ]

    func testDenominationRoundingPlain() {
        let rule: Decimal.RoundingMode = .plain
        roundingPlain.forEach { i in
            XCTAssertEqual(rounder.applyRounding(for: i.input, denomination: i.multiplier, roundingRule: rule), i.rounded)
        }
    }
}
