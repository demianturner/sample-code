// testing various HTTP responses

import XCTest
import OHHTTPStubs

@testable import Crypto_Tools_Pro

class CCPriceMultiServiceTests: XCTestCase {

    var client: CryptoCompareService!

    let fiatData: [SimpleCurrency] = DataManager.shared.fiatData
    let cryptoData: [SimpleCurrency] = DataManager.shared.cryptoData
    
    override func setUp() {
        super.setUp()
        OHHTTPStubs.onStubMissing { request in
            XCTFail("Missing stub for \(request)")
        }
        FixtureLoader.stubCoinListResponse()
        client = CryptoCompareService(session: URLSession.shared)
    }
    
    override func tearDown() {
        FixtureLoader.reset()
        client = nil
        super.tearDown()
    }

    func testFetchCoinListResponse() {
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in

            switch result {
            case .success(let coinList):
                exp.fulfill()
                XCTAssertNotNil(coinList)

            case .failure(let error):
                XCTFail("Error in coinlist request: \(error)")
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }

    func testCoinListResponseReturnsServerError() {
        FixtureLoader.stubCoinListReturningError()

        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            exp.fulfill()

            switch result {
            case .success(_):
                XCTFail("Should have returned an error")

            case .failure(let error):
                if case CCApiError.serverError(let status) = error {
                    XCTAssertEqual(status, 500)
                } else {
                    XCTFail("Expected server error but got \(error)")
                }
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }

    func testConnectionErrorIsReturned() {
        FixtureLoader.stubCoinListWithConnectionError(code: 999)
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            exp.fulfill()

            switch result {
            case .success(_):
                XCTFail("Should have failed")

            case .failure(let error):
                switch error {
                case .connectionError(let e):
                    XCTAssertEqual( (e as NSError).code, 999)
                default:
                    XCTFail("Expected a connection error but got \(error)")
                }
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }

    func testCatchesInvalidJSONOn200Response() {
        FixtureLoader.stubCoinListWithInvalidJSON()
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            exp.fulfill()

            switch result {
            case .success(_):
                XCTFail("Should have failed")

            case .failure(let error):
                switch error {
                case .responseFormatInvalid(let body):
                    XCTAssertEqual(body, "this is not valid json")
                default:
                    XCTFail("Expected a connection error but got \(error)")
                }
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }

    func testCatchesInvalidJSONWithNoBody() {
        let invalidData = Data(_: [UInt8.max])
        FixtureLoader.stubCoinListWithData(invalidData)
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            exp.fulfill()

            switch result {
            case .success(_):
                XCTFail("Should have failed")

            case .failure(let error):
                switch error {
                case .responseFormatInvalid(let body):
                    XCTAssertEqual(body, "<no body>")
                default:
                    XCTFail("Expected a connection error but got \(error)")
                }
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }
    
    // response is ""
    func testHandlesEmptyResponse() {
        FixtureLoader.stubCoinListWithAPILimitExceeded1()
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            exp.fulfill()

            switch result {
            case .success(_):
                XCTFail("Should have failed")

            case .failure(let error):
                switch error {
                case .responseFormatInvalid(let body):
                    XCTAssertEqual(body, "")
                default:
                    XCTFail("Expected a connection error but got \(error)")
                }
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }
        
    // response is "{}"
    func testHandlesEmptyJSON() {
        FixtureLoader.stubCoinListWithAPILimitExceeded2()
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            exp.fulfill()

            switch result {
            case .success(let data):
                XCTAssertEqual(data, CCResponsePriceMulti()) 

            case .failure(let error):
                XCTFail("Expected a empty response but got \(error)")
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }
    
    // response is "NULL"
    // https://benohead.com/blog/2018/01/15/null-valid-json-text/
    func testHandlesNullString() {
        FixtureLoader.stubCoinListWithAPILimitExceeded3()
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            exp.fulfill()

            switch result {
            case .success(_):
                XCTFail("Should have failed")

            case .failure(let error):
                switch error {
                case .responseFormatInvalid(let body):
                    XCTAssertEqual(body, "NULL\n")
                default:
                    XCTFail("Expected a connection error but got \(error)")
                }
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }

    func testCallsBackOnMainQueue() {
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            exp.fulfill()
            XCTAssert(Thread.isMainThread, "Expected to be called back on the main queue")
        }
        waitForExpectations(timeout: 4.0, handler: nil)
    }

    func testCorrectKeysAreReturned() {
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in

            switch result {
            case .success(let coinList):
                exp.fulfill()
                XCTAssertTrue(coinList["BTC"] != nil)
                XCTAssertTrue(coinList["ETH"] != nil)
                XCTAssertTrue(coinList["LTC"] != nil)

            case .failure(let error):
                XCTFail("Error in coinlist request: \(error)")
            }
        }
        waitForExpectations(timeout: 3.0, handler: nil)
    }
    
    func testAllKeysAndValuesAreReturned() {
        
        let exp = expectation(description: "We received the response")
        client.checkPriceMulti(symbols: ["BTC", "ETH", "LTC"], baseCurrency: "USD") { result in
            switch result {
            case .success(let coinList):
                exp.fulfill()
                
                XCTAssertNotNil(coinList)
                XCTAssert(coinList.count == 3)
                let firstCoin = coinList.first
                
                typealias CoinDictElem = (key: String, value: Dictionary<String, Double>?)
/*
▿ Optional((key: "BTC", value: ["USD": 6710.3999999999996]))
  ▿ some: (2 elements)
    - key: "BTC"
    ▿ value: 1 key/value pair
      ▿ (2 elements)
        - key: "USD"
        - value: 6710.3999999999996
 */
                XCTAssertNotNil(firstCoin?.key)
                XCTAssertNotNil(firstCoin?.value)
                let firstVal = firstCoin?.value.first
                XCTAssertNotNil(firstVal?.value) // price
                
            case .failure(let error):
                XCTFail("Error in coinlist request: \(error)")
            }
        }
        waitForExpectations(timeout: 2.0, handler: nil)
    }

    func testCanLoadCryptosFromDefaults() {
        XCTAssertNotNil(defaults.currencyList)
    }

    func testCryptosFromDefaultsContainValidSyms() {
        guard let defaultCurrencySymbolList = defaults.currencyList else {
            return
        }
        let validSyms = defaultCurrencySymbolList.filter { self.cryptoData.map { $0.code }.contains($0) }
        XCTAssertNotNil(validSyms)
    }

    func testCryptosFromDefaultsDoNotContainFiats() {
        guard let defaultCurrencySymbolList = defaults.currencyList else {
            return
        }
        let validSyms = defaultCurrencySymbolList.filter { self.cryptoData.map { $0.code }.contains($0) }
        let fiatCoins = self.fiatData.map { $0.code }
        validSyms.forEach { cryptoSym in
            XCTAssertTrue(!fiatCoins.contains(cryptoSym))
        }
    }
    
    // calculator
    func testEndpointPriceMulti() {
        let expected: Endpoint = .priceMulti(symbols: ["foo", "bar", "baz"], baseCurrency: "USD")
        let actual = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=foo,bar,baz&tsyms=foo,bar,baz&extraParams=crypto_calculator_macos&api_key=52a6d15f6abc2337bbab385a393e7720fdc73f49a824414580e3ff26071ab680"
        XCTAssertEqual(expected.url, URL(string: actual))
    }
    
    // ticker
    func testEndpointPriceMultiFull() {
        let expected: Endpoint = .priceMultiFull(symbols: ["foo", "bar", "baz"], baseCurrency: "USD")
        let actual = "https://min-api.cryptocompare.com/data/pricemultifull?fsyms=foo,bar,baz&tsyms=USD&extraParams=crypto_calculator_macos&api_key=52a6d15f6abc2337bbab385a393e7720fdc73f49a824414580e3ff26071ab680"
        XCTAssertEqual(expected.url, URL(string: actual))
    }
}
