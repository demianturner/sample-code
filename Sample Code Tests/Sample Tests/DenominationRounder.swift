import Foundation

public struct DenominationRounder {

    public init() {}

    public enum Multiplier: Decimal {
        case none = 0
        case hundredth = 0.01
        case half = 0.5
        case integral = 1.0
        case two = 2.0
        case five = 5.0
        case fifty = 50.0

        init(_ decimal: Decimal) {
            switch decimal {
            case 0:
                self = .none
            case 0.01:
                self = .hundredth
            case 0.5:
                self = .half
            case 1.0:
                self = .integral
            case 2.0:
                self = .two
            case 5.0:
                self = .five
            case 50:
                self = .fifty
            default:
                self = .none
            }
        }
    }

    public func deriveMultiplier(from lowestDenomination: Decimal = 0.0) -> Multiplier {
        return Multiplier(lowestDenomination)
    }

    public func applyRounding(for amount: Decimal, denomination: Multiplier, roundingRule: Decimal.RoundingMode) -> Decimal {
        guard denomination != .none else { return amount }
        return (amount / denomination.rawValue).rounded(places: 0, rounding: roundingRule) * denomination.rawValue
    }
}

public extension Currency {
    func amountIsMultipleOfLowestDenomination(_ amount: Decimal) -> Bool {
        let multiplier = DenominationRounder().deriveMultiplier(from: lowestDenomination)
        let unexceptedMultipliers: [DenominationRounder.Multiplier] = [.none, .hundredth]

        guard !unexceptedMultipliers.contains(multiplier) else {
            return true
        }

        let ld = (lowestDenomination as NSDecimalNumber).doubleValue
        let amount = (amount as NSDecimalNumber).doubleValue
        return amount.truncatingRemainder(dividingBy: ld) == 0.0
    }
}
