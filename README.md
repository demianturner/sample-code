#  Sample code by Demian Turner

PLEASE NOTE: All code dependencies have not been included so code does not compile.

Please refer to 

## code examples 

SEE: Sample Code Project > Sample Code

- data validation
- sample MVVM
- web service class, endpoints, response decoding
- custom view with autolayout, "visual format language"

## unit tests examples

SEE: Sample Code Tests > Sample Tests

- testing rounding
- testing various HTTP responses
